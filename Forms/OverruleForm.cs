﻿using System;

namespace CipherLAB_simulatie.Forms
{
    public partial class OverruleForm : FormTemplate
    {
        public OverruleForm()
        {
            InitializeComponent();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
