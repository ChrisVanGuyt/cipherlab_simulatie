﻿using CipherLAB_simulatie.Calculations;
using System;
using System.Windows.Forms;

namespace CipherLAB_simulatie.Forms
{
    public partial class HomeForm : FormTemplate
    {
        public HomeForm()
        {
            InitializeComponent();
            ShowEmployeeInStatusBar = false;
            ShowHomeButton = false;
            Title = "Hoofdmenu";
        }

        private void Exit_Click(object sender, EventArgs e) => Close();

        private readonly object[] _menuOptions = { typeof(StartMasterProductionOrderForm), typeof(SelectRawMaterialForm), typeof(ProductionOrderFinishedForm), typeof(WeighScrapForm), null, null };

        private void MenuButton1_Click(object sender, EventArgs e) => Activate(1);

        private void MenuButton2_Click(object sender, EventArgs e) => Activate(2);

        private void MenuButton3_Click(object sender, EventArgs e) => Activate(3);

        private void MenuButton4_Click(object sender, EventArgs e) => Activate(4);

        private void MenuButton5_Click(object sender, EventArgs e) => Activate(5);

        private void MenuButton6_Click(object sender, EventArgs e) => Activate(6);

        private void Activate(int menuAction)
        {
            if (_menuOptions.Length < menuAction)
            {
                throw new NotImplementedException($"Geen actie toegekend aan knop {menuAction}.");
            }

            switch (_menuOptions[menuAction - 1])
            {
                case Type type:
                    ActivateScreen(type);
                    break;
                case null:
                    break;
                default:
                    throw new NotImplementedException($"Menu action {_menuOptions[menuAction]} not implemented.");
            }
        }

        private void ActivateScreen(Type type)
        {
            FormTemplate o = Activator.CreateInstance(type) as FormTemplate;
            o.Employee = EmployeeTextBox.Text;
            o.StartPosition = FormStartPosition.Manual;
            o.Location = Location;
            o.ShowDialog();
        }

        private void DisableMenuButtons() => EnableMenuButtons(false);

        private void EnableMenuButtons() => EnableMenuButtons(true);

        private void EnableMenuButtons(bool v) => MenuButton1.Enabled = MenuButton2.Enabled = MenuButton3.Enabled = MenuButton4.Enabled = MenuButton5.Enabled = MenuButton6.Enabled = v;

        private void EmployeeTextBox_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string text = EmployeeTextBox.Text.Trim();

            if (int.TryParse(text, out int nr) && nr == 14)
            {
                EmployeeTextBox.Text = "Jan";
                EnableMenuButtons();
                return;
            }

            if (LevenshteinDistance.Compute(text, "Jan", LevenshteinDistanceOptions.IgnoreCase) <= (text.Length + 4) / 5)
            {
                EmployeeTextBox.Text = "Jan";
                EnableMenuButtons();
                return;
            }

            new MessageBoxForm($"'{text}' is geen geldige gebruiker.").ShowDialog();
            DisableMenuButtons();
        }

        private void CloseButton_Click(object sender, EventArgs e) => Close();

        private void HomeForm_MouseDown(object sender, MouseEventArgs e) => FormTemplate_MouseDown(sender, e);
    }
}
