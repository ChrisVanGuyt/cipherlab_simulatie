﻿using System;
using System.Windows.Forms;

namespace CipherLAB_simulatie.Forms
{
    public enum MessageBoxFormButtons
    {
        Yes,
        YesNo
    }

    public partial class MessageBoxForm : FormTemplate
    {
        public MessageBoxForm(string message) : this(message, MessageBoxFormButtons.Yes) { }

        public MessageBoxForm(string message, MessageBoxFormButtons buttons)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentException($"'{nameof(message)}' cannot be null or whitespace", nameof(message));
            }

            InitializeComponent();
            MessageTextBox.Text = message;

            switch (buttons)
            {
                case MessageBoxFormButtons.Yes:
                    NoButton.Visible = false;
                    YesButton.Location = NoButton.Location;
                    break;
                case MessageBoxFormButtons.YesNo:
                    break;
                default:
                    throw new NotImplementedException($"{nameof(buttons)} value {buttons} not implemented.");
            }
        }

        private void NoButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
            Close();
        }

        private void YesButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Yes;
            Close();
        }
    }
}
