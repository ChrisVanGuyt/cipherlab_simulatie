﻿namespace CipherLAB_simulatie.Forms
{
    public partial class ProductionOrderFinishedForm : FormTemplate
    {
        public ProductionOrderFinishedForm()
        {
            InitializeComponent();
            Title = "Prod.order klaar";
        }
    }
}
