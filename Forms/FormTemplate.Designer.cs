﻿namespace CipherLAB_simulatie.Forms
{
    partial class FormTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TitleLabel = new System.Windows.Forms.Label();
            this.StatusPictureBox = new System.Windows.Forms.PictureBox();
            this.HomeButton = new System.Windows.Forms.Button();
            this.EmployeeTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.StatusPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLabel.Location = new System.Drawing.Point(30, 0);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(0, 24);
            this.TitleLabel.TabIndex = 14;
            this.TitleLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormTemplate_MouseDown);
            // 
            // StatusPictureBox
            // 
            this.StatusPictureBox.Enabled = false;
            this.StatusPictureBox.Image = global::CipherLAB_simulatie.Properties.Resources.ok_24x;
            this.StatusPictureBox.Location = new System.Drawing.Point(216, 0);
            this.StatusPictureBox.Name = "StatusPictureBox";
            this.StatusPictureBox.Size = new System.Drawing.Size(24, 24);
            this.StatusPictureBox.TabIndex = 19;
            this.StatusPictureBox.TabStop = false;
            this.StatusPictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormTemplate_MouseDown);
            // 
            // HomeButton
            // 
            this.HomeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HomeButton.Image = global::CipherLAB_simulatie.Properties.Resources.Home_16x;
            this.HomeButton.Location = new System.Drawing.Point(0, 0);
            this.HomeButton.Name = "HomeButton";
            this.HomeButton.Size = new System.Drawing.Size(24, 24);
            this.HomeButton.TabIndex = 18;
            this.HomeButton.UseVisualStyleBackColor = true;
            this.HomeButton.Click += new System.EventHandler(this.HomeButton_Click);
            // 
            // EmployeeTextBox
            // 
            this.EmployeeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmployeeTextBox.Location = new System.Drawing.Point(0, 289);
            this.EmployeeTextBox.Name = "EmployeeTextBox";
            this.EmployeeTextBox.ReadOnly = true;
            this.EmployeeTextBox.Size = new System.Drawing.Size(240, 31);
            this.EmployeeTextBox.TabIndex = 20;
            this.EmployeeTextBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormTemplate_MouseDown);
            // 
            // FormTemplate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.EmployeeTextBox);
            this.Controls.Add(this.StatusPictureBox);
            this.Controls.Add(this.HomeButton);
            this.Controls.Add(this.TitleLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormTemplate";
            this.ShowIcon = false;
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormTemplate_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.StatusPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.PictureBox StatusPictureBox;
        private System.Windows.Forms.Button HomeButton;
        private System.Windows.Forms.TextBox EmployeeTextBox;
    }
}

