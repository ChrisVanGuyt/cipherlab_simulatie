﻿namespace CipherLAB_simulatie.Forms
{
    partial class MessageBoxForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MessageTextBox = new System.Windows.Forms.TextBox();
            this.NoButton = new System.Windows.Forms.Button();
            this.YesButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MessageTextBox
            // 
            this.MessageTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MessageTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MessageTextBox.Location = new System.Drawing.Point(12, 30);
            this.MessageTextBox.Multiline = true;
            this.MessageTextBox.Name = "MessageTextBox";
            this.MessageTextBox.ReadOnly = true;
            this.MessageTextBox.Size = new System.Drawing.Size(216, 186);
            this.MessageTextBox.TabIndex = 10;
            this.MessageTextBox.Text = "Artikel \"neva3\" niet gevonden.  Bedoel je \"NOVA3\"?";
            // 
            // NoButton
            // 
            this.NoButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.NoButton.Font = new System.Drawing.Font("Segoe MDL2 Assets", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NoButton.ForeColor = System.Drawing.Color.Red;
            this.NoButton.Location = new System.Drawing.Point(123, 215);
            this.NoButton.Name = "NoButton";
            this.NoButton.Size = new System.Drawing.Size(105, 68);
            this.NoButton.TabIndex = 9;
            this.NoButton.Text = "";
            this.NoButton.UseVisualStyleBackColor = true;
            this.NoButton.Click += new System.EventHandler(this.NoButton_Click);
            // 
            // YesButton
            // 
            this.YesButton.Font = new System.Drawing.Font("Segoe MDL2 Assets", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YesButton.ForeColor = System.Drawing.Color.Green;
            this.YesButton.Location = new System.Drawing.Point(12, 215);
            this.YesButton.Name = "YesButton";
            this.YesButton.Size = new System.Drawing.Size(105, 68);
            this.YesButton.TabIndex = 11;
            this.YesButton.Text = "";
            this.YesButton.UseVisualStyleBackColor = true;
            this.YesButton.Click += new System.EventHandler(this.YesButton_Click);
            // 
            // MessageBoxForm
            // 
            this.AcceptButton = this.YesButton;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.NoButton;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.YesButton);
            this.Controls.Add(this.MessageTextBox);
            this.Controls.Add(this.NoButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MessageBoxForm";
            this.ShowEmployeeInStatusBar = true;
            this.ShowHomeButton = true;
            this.Controls.SetChildIndex(this.NoButton, 0);
            this.Controls.SetChildIndex(this.MessageTextBox, 0);
            this.Controls.SetChildIndex(this.YesButton, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button NoButton;
        private System.Windows.Forms.TextBox MessageTextBox;
        private System.Windows.Forms.Button YesButton;
    }
}

