﻿namespace CipherLAB_simulatie.Forms
{
    partial class HomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EmployeeLabel = new System.Windows.Forms.Label();
            this.EmployeeTextBox = new System.Windows.Forms.TextBox();
            this.MenuButton5 = new System.Windows.Forms.Button();
            this.MenuButton6 = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.MenuButton4 = new System.Windows.Forms.Button();
            this.MenuButton3 = new System.Windows.Forms.Button();
            this.MenuButton2 = new System.Windows.Forms.Button();
            this.MenuButton1 = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // EmployeeLabel
            // 
            this.EmployeeLabel.AutoSize = true;
            this.EmployeeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmployeeLabel.Location = new System.Drawing.Point(8, 37);
            this.EmployeeLabel.Name = "EmployeeLabel";
            this.EmployeeLabel.Size = new System.Drawing.Size(93, 24);
            this.EmployeeLabel.TabIndex = 0;
            this.EmployeeLabel.Text = "Gebruiker";
            this.EmployeeLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.HomeForm_MouseDown);
            // 
            // EmployeeTextBox
            // 
            this.EmployeeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmployeeTextBox.Location = new System.Drawing.Point(12, 64);
            this.EmployeeTextBox.Name = "EmployeeTextBox";
            this.EmployeeTextBox.Size = new System.Drawing.Size(216, 38);
            this.EmployeeTextBox.TabIndex = 1;
            this.EmployeeTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.EmployeeTextBox_Validating);
            // 
            // MenuButton5
            // 
            this.MenuButton5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuButton5.Image = global::CipherLAB_simulatie.Properties.Resources.test_48x;
            this.MenuButton5.Location = new System.Drawing.Point(86, 240);
            this.MenuButton5.Name = "MenuButton5";
            this.MenuButton5.Size = new System.Drawing.Size(68, 68);
            this.MenuButton5.TabIndex = 6;
            this.MenuButton5.UseVisualStyleBackColor = true;
            this.MenuButton5.Click += new System.EventHandler(this.MenuButton5_Click);
            // 
            // MenuButton6
            // 
            this.MenuButton6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuButton6.Location = new System.Drawing.Point(160, 240);
            this.MenuButton6.Name = "MenuButton6";
            this.MenuButton6.Size = new System.Drawing.Size(68, 68);
            this.MenuButton6.TabIndex = 7;
            this.MenuButton6.Text = ">>>";
            this.MenuButton6.UseVisualStyleBackColor = true;
            this.MenuButton6.Click += new System.EventHandler(this.MenuButton6_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ExitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitButton.Image = global::CipherLAB_simulatie.Properties.Resources.Close_red_16x;
            this.ExitButton.Location = new System.Drawing.Point(0, 0);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(24, 24);
            this.ExitButton.TabIndex = 8;
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.Exit_Click);
            // 
            // MenuButton4
            // 
            this.MenuButton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuButton4.Image = global::CipherLAB_simulatie.Properties.Resources.Scrap_48x;
            this.MenuButton4.Location = new System.Drawing.Point(12, 240);
            this.MenuButton4.Name = "MenuButton4";
            this.MenuButton4.Size = new System.Drawing.Size(68, 68);
            this.MenuButton4.TabIndex = 5;
            this.MenuButton4.UseVisualStyleBackColor = true;
            this.MenuButton4.Click += new System.EventHandler(this.MenuButton4_Click);
            // 
            // MenuButton3
            // 
            this.MenuButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuButton3.Image = global::CipherLAB_simulatie.Properties.Resources.StopProduction_48x;
            this.MenuButton3.Location = new System.Drawing.Point(160, 166);
            this.MenuButton3.Name = "MenuButton3";
            this.MenuButton3.Size = new System.Drawing.Size(68, 68);
            this.MenuButton3.TabIndex = 4;
            this.MenuButton3.UseVisualStyleBackColor = true;
            this.MenuButton3.Click += new System.EventHandler(this.MenuButton3_Click);
            // 
            // MenuButton2
            // 
            this.MenuButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuButton2.Image = global::CipherLAB_simulatie.Properties.Resources.BigBag_48x;
            this.MenuButton2.Location = new System.Drawing.Point(86, 166);
            this.MenuButton2.Name = "MenuButton2";
            this.MenuButton2.Size = new System.Drawing.Size(68, 68);
            this.MenuButton2.TabIndex = 3;
            this.MenuButton2.UseVisualStyleBackColor = true;
            this.MenuButton2.Click += new System.EventHandler(this.MenuButton2_Click);
            // 
            // MenuButton1
            // 
            this.MenuButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuButton1.Image = global::CipherLAB_simulatie.Properties.Resources.StartProduction_48x;
            this.MenuButton1.Location = new System.Drawing.Point(12, 166);
            this.MenuButton1.Name = "MenuButton1";
            this.MenuButton1.Size = new System.Drawing.Size(68, 68);
            this.MenuButton1.TabIndex = 2;
            this.MenuButton1.UseVisualStyleBackColor = true;
            this.MenuButton1.Click += new System.EventHandler(this.MenuButton1_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseButton.Image = global::CipherLAB_simulatie.Properties.Resources.Close_red_16x;
            this.CloseButton.Location = new System.Drawing.Point(0, 0);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(24, 24);
            this.CloseButton.TabIndex = 21;
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // HomeForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.ExitButton;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.MenuButton6);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.MenuButton5);
            this.Controls.Add(this.MenuButton4);
            this.Controls.Add(this.MenuButton3);
            this.Controls.Add(this.MenuButton2);
            this.Controls.Add(this.MenuButton1);
            this.Controls.Add(this.EmployeeTextBox);
            this.Controls.Add(this.EmployeeLabel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HomeForm";
            this.ShowEmployeeInStatusBar = true;
            this.ShowHomeButton = true;
            this.Controls.SetChildIndex(this.EmployeeLabel, 0);
            this.Controls.SetChildIndex(this.EmployeeTextBox, 0);
            this.Controls.SetChildIndex(this.MenuButton1, 0);
            this.Controls.SetChildIndex(this.MenuButton2, 0);
            this.Controls.SetChildIndex(this.MenuButton3, 0);
            this.Controls.SetChildIndex(this.MenuButton4, 0);
            this.Controls.SetChildIndex(this.MenuButton5, 0);
            this.Controls.SetChildIndex(this.ExitButton, 0);
            this.Controls.SetChildIndex(this.MenuButton6, 0);
            this.Controls.SetChildIndex(this.CloseButton, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label EmployeeLabel;
        private System.Windows.Forms.TextBox EmployeeTextBox;
        private System.Windows.Forms.Button MenuButton1;
        private System.Windows.Forms.Button MenuButton2;
        private System.Windows.Forms.Button MenuButton3;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Button MenuButton5;
        private System.Windows.Forms.Button MenuButton4;
        private System.Windows.Forms.Button MenuButton6;
        private System.Windows.Forms.Button CloseButton;
    }
}

