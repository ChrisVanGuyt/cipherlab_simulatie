﻿namespace CipherLAB_simulatie.Forms
{
    partial class ProductionOrderFinishedForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.QuantityTextBox = new System.Windows.Forms.TextBox();
            this.QuantityLabel = new System.Windows.Forms.Label();
            this.ProductionOrderTextBox = new System.Windows.Forms.TextBox();
            this.ProductionOrderLabel = new System.Windows.Forms.Label();
            this.LineTextBox = new System.Windows.Forms.TextBox();
            this.LineLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // QuantityTextBox
            // 
            this.QuantityTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuantityTextBox.Location = new System.Drawing.Point(12, 210);
            this.QuantityTextBox.Name = "QuantityTextBox";
            this.QuantityTextBox.Size = new System.Drawing.Size(215, 38);
            this.QuantityTextBox.TabIndex = 20;
            // 
            // QuantityLabel
            // 
            this.QuantityLabel.AutoSize = true;
            this.QuantityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuantityLabel.Location = new System.Drawing.Point(12, 183);
            this.QuantityLabel.Name = "QuantityLabel";
            this.QuantityLabel.Size = new System.Drawing.Size(62, 24);
            this.QuantityLabel.TabIndex = 19;
            this.QuantityLabel.Text = "Aantal";
            // 
            // ProductionOrderTextBox
            // 
            this.ProductionOrderTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductionOrderTextBox.Location = new System.Drawing.Point(12, 137);
            this.ProductionOrderTextBox.Name = "ProductionOrderTextBox";
            this.ProductionOrderTextBox.Size = new System.Drawing.Size(215, 38);
            this.ProductionOrderTextBox.TabIndex = 18;
            // 
            // ProductionOrderLabel
            // 
            this.ProductionOrderLabel.AutoSize = true;
            this.ProductionOrderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductionOrderLabel.Location = new System.Drawing.Point(12, 110);
            this.ProductionOrderLabel.Name = "ProductionOrderLabel";
            this.ProductionOrderLabel.Size = new System.Drawing.Size(135, 24);
            this.ProductionOrderLabel.TabIndex = 17;
            this.ProductionOrderLabel.Text = "Productieorder";
            // 
            // LineTextBox
            // 
            this.LineTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LineTextBox.Location = new System.Drawing.Point(11, 64);
            this.LineTextBox.Name = "LineTextBox";
            this.LineTextBox.Size = new System.Drawing.Size(216, 38);
            this.LineTextBox.TabIndex = 16;
            // 
            // LineLabel
            // 
            this.LineLabel.AutoSize = true;
            this.LineLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LineLabel.Location = new System.Drawing.Point(12, 37);
            this.LineLabel.Name = "LineLabel";
            this.LineLabel.Size = new System.Drawing.Size(39, 24);
            this.LineLabel.TabIndex = 15;
            this.LineLabel.Text = "Lijn";
            // 
            // ProductionOrderFinishedForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.Controls.Add(this.QuantityTextBox);
            this.Controls.Add(this.QuantityLabel);
            this.Controls.Add(this.ProductionOrderTextBox);
            this.Controls.Add(this.ProductionOrderLabel);
            this.Controls.Add(this.LineTextBox);
            this.Controls.Add(this.LineLabel);
            this.Name = "ProductionOrderFinishedForm";
            this.Controls.SetChildIndex(this.LineLabel, 0);
            this.Controls.SetChildIndex(this.LineTextBox, 0);
            this.Controls.SetChildIndex(this.ProductionOrderLabel, 0);
            this.Controls.SetChildIndex(this.ProductionOrderTextBox, 0);
            this.Controls.SetChildIndex(this.QuantityLabel, 0);
            this.Controls.SetChildIndex(this.QuantityTextBox, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox QuantityTextBox;
        private System.Windows.Forms.Label QuantityLabel;
        private System.Windows.Forms.TextBox ProductionOrderTextBox;
        private System.Windows.Forms.Label ProductionOrderLabel;
        private System.Windows.Forms.TextBox LineTextBox;
        private System.Windows.Forms.Label LineLabel;
    }
}
