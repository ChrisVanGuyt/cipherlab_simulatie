﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace CipherLAB_simulatie.Forms
{
    public partial class FormTemplate : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        public string Employee
        {
            get => EmployeeTextBox.Text;
            set => EmployeeTextBox.Text = value.Trim();
        }

        public string Title
        {
            get => TitleLabel.Text;
            set => TitleLabel.Text = value.Trim();
        }

        public bool ShowEmployeeInStatusBar
        {
            get => EmployeeTextBox.Visible;
            set => EmployeeTextBox.Visible = value;
        }

        public bool ShowHomeButton
        {
            get => HomeButton.Visible;
            set => HomeButton.Visible = value;
        }

        public FormTemplate()
        {
            InitializeComponent();
        }

        private void HomeButton_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Make the borderless window movable
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <see cref="https://stackoverflow.com/questions/1592876/make-a-borderless-form-movable/1592899"/>
        public void FormTemplate_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && Name is nameof(HomeForm))
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
    }
}
