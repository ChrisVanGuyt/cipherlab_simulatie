﻿using System;

namespace CipherLAB_simulatie.Calculations
{
    public enum LevenshteinDistanceOptions 
    {
        CaseSensitive,
        IgnoreCase,
    }

    class LevenshteinDistance
    {
        public static int Compute(string strA, string strB, LevenshteinDistanceOptions options)
        {
            switch (options)
            {
                case LevenshteinDistanceOptions.CaseSensitive:
                    return LevenshteinDistance.Compute(strA, strB);
                case LevenshteinDistanceOptions.IgnoreCase:
                    return LevenshteinDistance.Compute(strA.ToLowerInvariant(), strB.ToLowerInvariant());
                default:
                    throw new NotImplementedException($"{nameof(options)} not implemented.");
            }
        }

        public static int Compute(string strA, string strB)
        {
            int n = strA.Length;
            int m = strB.Length;
            int[,] d = new int[n + 1, m + 1];

            // Verify arguments.
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Initialize arrays.
            for (int i = 0; i <= n; i++)
            {
                d[i, 0] = i;
            }

            for (int j = 0; j <= m; j++)
            {
                d[0, j] = j;
            }

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    int cost = (strB[j - 1] == strA[i - 1]) ? 0 : 1;
                    d[i, j] = Math.Min(Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1), d[i - 1, j - 1] + cost);
                }
            }

            return d[n, m];
        }
    }
}
